﻿
using NServiceBus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using NServiceBus.Logging;

namespace NSBWcfBackend
{
    public class OrderStorageService : IOrderStorageService
    {
        private static readonly ILog Log= LogManager.GetLogger<OrderStorageService>();
        private readonly IEndpointInstance _endpoint;
        public OrderStorageService(IEndpointInstance endpoint)
        {
            _endpoint = endpoint;
        }
        public Guid SaveOrder(Order order)
        {
            if (order == null)
            {
                throw new ArgumentNullException("Order");
            }
            if (order.OrderMessage == null)
            {
                throw new ArgumentNullException("order.OrderMessage");
            }
            if (order.OrderMessage.Type != Shared.MessageType.Storage)
            {
                throw new ArgumentNullException(order.OrderMessage.Type + "is incompatible with order storage service. Only message of type Storage can be processed.");
            }
            try
            {
                order.Id = Guid.NewGuid();
                Log.Info(string.Format("Order Message recieved. ID:{0} ", order.Id));
                _endpoint.Send("Samples.Wcf.Endpoint", order.OrderMessage).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                Log.Info("ServiceBus.Send throw Error:");
                Log.Error("NServiceBus.Send throw Error:", ex);
                throw;
            }

            return order.Id;
        }

    }
}
