﻿using Autofac;
using Autofac.Integration.Wcf;

using NServiceBus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using NServiceBus.Logging;

namespace NSBWcfBackend
{
    public class Global : System.Web.HttpApplication
    {
        private IEndpointInstance _endpoint;
        
        protected void Application_Start(object sender, EventArgs e)
        {  // build and set container in application start
            var builder = new ContainerBuilder();
            // register types    
            _endpoint = ConfigureServiceBus();
            builder.RegisterType<OrderStorageService>().As<IOrderStorageService>();
           
            // Register NSB Endpoint instance
            builder.RegisterInstance(_endpoint);
            builder.RegisterInstance(new OrderStorageMessageHandler());
            
            builder.RegisterAssemblyTypes(typeof(OrderStorageService).Assembly);
            IContainer container = builder.Build();
            AutofacHostFactory.Container = container;
        }
      
        private  IEndpointInstance ConfigureServiceBus()
        {
            var endpointConfiguration = new EndpointConfiguration("Samples.Wcf.Endpoint");

            endpointConfiguration.UseSerialization<JsonSerializer>();
            endpointConfiguration.UsePersistence<InMemoryPersistence>();
            endpointConfiguration.UseTransport<MsmqTransport>();
            endpointConfiguration.SendFailedMessagesTo("Samples.Wcf.Endpoint.Error");

            var recoverability = endpointConfiguration.Recoverability();
            recoverability.Delayed(
                delayed =>
                {
                    delayed.NumberOfRetries(2);
                    delayed.TimeIncrease(TimeSpan.FromSeconds(2));
                });
            endpointConfiguration.EnableInstallers();
            _endpoint = Endpoint.Start(endpointConfiguration).GetAwaiter().GetResult();
            return _endpoint;
        }
        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}