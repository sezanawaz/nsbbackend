﻿using System.Threading.Tasks;
using System.Web.Script.Serialization;
using NServiceBus;
using NServiceBus.Logging;
using Shared;

namespace NSBWcfBackend
{
    public class OrderStorageMessageHandler : IHandleMessages<ContactMessage>
    {
        static ILog log = LogManager.GetLogger<OrderStorageMessageHandler>();

        public Task Handle(ContactMessage message, IMessageHandlerContext context)
        {
            log.Info("Hello from OrderStorageMessageHandler");
            var json = new JavaScriptSerializer().Serialize(message);
            log.Info("Message Recieved: " + json);
            //if (message.OrderId != null)
            //{
            //    throw new Exception("Error in Message");            
            //}
            return Task.CompletedTask;
        }
    }
}